# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=jql
pkgver=6.0.6
pkgrel=0
pkgdesc="A JSON Query Language CLI tool"
url="https://github.com/yamafaktory/jql"
arch="all"
license="MIT"
makedepends="cargo"
source="https://github.com/yamafaktory/jql/archive/jql-v$pkgver.tar.gz"
options="net" # fetch dependencies
builddir="$srcdir/jql-jql-v$pkgver"

[ "$CARCH" = "riscv64" ] && options="$options textrels"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release
}

check() {
	# core::tests::get_property_as_index seems to be broken
	cargo test --frozen -- \
		--skip core::tests::get_property_as_index
}

package() {
	install -D -m755 target/release/jql -t "$pkgdir"/usr/bin/
}

sha512sums="
a07ffb435ffe72b4b43aaa229ad06ef9feb22b1cbcf8c0725780ef9841df395729cba65a3ee080255c668cf0ec0453d089cc1095485612ba813aafc49daada92  jql-v6.0.6.tar.gz
"
