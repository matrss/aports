# Contributor: Thomas Deutsch <thomas@tuxpeople.org>
# Maintainer: Thomas Deutsch <thomas@tuxpeople.org>
pkgname=lazygit
pkgver=0.38.1
pkgrel=1
pkgdesc="Simple terminal UI for git commands"
url="https://github.com/jesseduffield/lazygit"
arch="all"
license="MIT"
# bash: for running git commands
depends="ncurses bash"
makedepends="go"
options="!check" # FIXME: https://github.com/jesseduffield/lazygit/issues/1009
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jesseduffield/lazygit/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags="-X main.version=$pkgver" -v
}

package() {
	install -Dm0755 $pkgname -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/doc
	mv docs "$pkgdir"/usr/share/doc/"$pkgname"
	install -Dm0644 LICENSE -t "$pkgdir"/usr/share/licenses/"$pkgname"
}

sha512sums="
1fcb7686d6b7b711005200e335763a56874b1aa2f692a4638b06a0550d62b6a816dd2b40e151b35b4d02b1ae7272f2f464a2fe87a33c1ae7179894dc72c7ddcc  lazygit-0.38.1.tar.gz
"
