# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-libvirt
_pkgname=libvirt-python
pkgver=9.3.0
pkgrel=0
pkgdesc="The libvirt virtualization API python binding"
url="https://libvirt.org/"
arch="all"
license="LGPL-2.0-or-later"
makedepends="
	libvirt-dev
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest-xdist py3-lxml"
subpackages="$pkgname-pyc"
source="https://libvirt.org/sources/python/$_pkgname-$pkgver.tar.gz
	disable-screenshot-test.patch"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-libvirt" # Backwards compatibility
provides="py-libvirt=$pkgver" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
5fe04eb6fbccb20f90637d5bb74887b0676743761838c91f6c7f08f8aff39ea8e1d8cdc50c4c244918c2100efb00c6bb380ac44b4d403a08863c8c99ecdbe937  libvirt-python-9.3.0.tar.gz
3d1d62a5d5283e5679dd1318a335b2785c895499f1fedae5d75947e29b0a55f6e4925108cf74e6b36f1c9f140aaf5c89171b84d8e94b1c3d3fc6fdc8c7f2f5fc  disable-screenshot-test.patch
"
