# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-tldextract
_pyname=tldextract
pkgver=3.4.1
pkgrel=0
pkgdesc="Accurately separate the TLD from the registered domain and subdomains of a URL"
url="https://github.com/john-kurkowski/tldextract"
arch="noarch"
license="BSD-3-Clause"
depends="
	python3
	py3-idna
	py3-requests
	py3-requests-file
	py3-filelock
	"
makedepends="py3-gpep517 py3-setuptools py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest py3-pytest-mock py3-responses"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/john-kurkowski/tldextract/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/*.whl
}
sha512sums="
6918ad59daeb002fc58656537578d0f977272169a96f31fe57c3a02c25eaacf2d3fd6e8907e3e215fecf2ea860b4c5ca1624f9f5c834e6f94fe8fcd60ae6fbdc  tldextract-3.4.1.tar.gz
"
